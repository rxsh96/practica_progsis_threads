#include "csapp.h"
#include "sha256.h"
#include <getopt.h>
#include <pthread.h>

void hash(int connfd);
void *thread(void *vargp);
int daemonize();

//Variables globales que determinan si una opción esta siendo usada
int nflag = 0; //Opción -n, ignora los saltos de línea
int pflag = 0; //Opción -p, especifica el puerto
int dflag = 0; //Opción -d corre el servidor como daemon

int main(int argc, char **argv){
	
	int listenfd, *connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;
	int c;

	pthread_t tid;

	while((c = getopt (argc, argv, "dnp:")) != -1){
		switch(c){
			case 'n':
				nflag = 1;
				break;
			case 'p':
				pflag = 1;
				port = optarg;
				break;
			case 'd':
				dflag = 1;
				break;
			case '?':
			default:
				fprintf(stderr, "uso: %s [-d] [-n] -p <port>\n", argv[0]);
				return -1;
		}
	}

	//Opción -p es obligatoria
	if(!pflag){
		fprintf(stderr, "uso: %s [-d] [-n] -p <port>\n", argv[0]);
		return -1;
	}


	listenfd = Open_listenfd(port);
	if(dflag){
		daemonize();
	}
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Malloc(sizeof(int));
		*connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		Pthread_create(&tid, NULL, thread, connfd);
	}
	exit(0);
}


void *thread(void *vargp){

	int connfd = *((int *)vargp);
	Pthread_detach(pthread_self());
	Free(vargp);
	hash(connfd);
	Close(connfd);
	return NULL;

}

//Procesa los datos enviados por el cliente
void hash(int connfd){
	
	size_t n;
	char buf[MAXLINE];
	BYTE sha256_buf[SHA256_BLOCK_SIZE];
	rio_t rio;
	SHA256_CTX ctx;
	
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		printf("hashing: %s", buf);

		//Ignora '\n' asumiendo que esta al final de buf
		if(nflag){
			n--;
		}
		//hash sha256
		sha256_init(&ctx);
		sha256_update(&ctx, (BYTE *) buf, n);
		sha256_final(&ctx, sha256_buf);

		Rio_writen(connfd, sha256_buf, SHA256_BLOCK_SIZE);
	}
}
